const Employee = require('../../models/user');

module.exports.getEmployees = async event => {

  try {
    const employees = await Employee.findAll();
    return {
      statusCode: 200,
      body: JSON.stringify({
          employees: employees
      })
    };
  } catch (error) {
    console.error('Unable to fetch employees', error);
    return {
      statusCode: 500,
      body: JSON.stringify(
        {
          message: 'Unable to fetch employees',
          error: error,
        },
        null,
        2
      ),
    };
  }

};
