const Employee = require('../../models/user');

module.exports.createEmployee = async event => {
    console.log("EVENT: ", event)
    const body = JSON.parse(event.body)
    const employee = { Name: body.name, Salary: body.salary};
    try {
      await Employee.create(employee);
      console.log('Employee created.');
      return {
        statusCode: 200,
        body: JSON.stringify(
          {
            message: 'Employee created.',
          },
          null,
          2
        ),
      };
    } catch (error) {
      console.error('Unable to create employee', error);
      return {
        statusCode: 500,
        body: JSON.stringify(
          {
            message: 'Unable to create employee',
            error: error,
          },
          null,
          2
        ),
      };
    }
  
  };