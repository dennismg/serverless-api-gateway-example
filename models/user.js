const { Sequelize, DataTypes, Model } = require('sequelize');
class Employee extends Model {
}

const sequelize = new Sequelize(
    'unwind', 
    'unwind', 
    'unwind123', {
    host: 'unwind-dev-instance-1.c0xjitcfqke4.us-east-1.rds.amazonaws.com',
    port: 5432,
    dialect: 'postgres'
  });

  Employee.init({
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: Sequelize.UUIDV4
    },
    Name: {
      type: DataTypes.STRING
      // allowNull defaults to true
    },
    Salary: {
      type: DataTypes.DOUBLE
    }
  }, {
    // Other model options go here
    timestamps: false,
    freezeTableName: true,
    sequelize, // We need to pass the connection instance
    modelName: 'Employee' // We need to choose the model name
  });
  

  module.exports = Employee;